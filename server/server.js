import _ from 'lodash'
import bodyParser from 'body-parser'
import express from 'express'
import {Contact} from './db'

const srv = express();
const port = process.env.PORT || 1234;


srv.use(bodyParser.json())

srv.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

srv.get('/contacts', async (req, res) => {
  res.send(await Contact.query())
})

srv.post('/contacts', async (req, res) => {
  await Contact.query().insert(req.body)
  res.status(204).end()
})

srv.get('/contacts/:id', async (req, res) => {
  res.send(await Contact.findById(req.params.id))
})

srv.post('/contacts/:id', async (req, res) => {
  const c = await Contact.findById(req.params.id)

  await c.$query().update(req.body)
  res.status(204).end()
})

srv.delete('/contacts/:id', async (req, res) => {
  const c = await Contact.findById(req.params.id)

  await c.$query().delete()
  res.status(204).end()
})

srv.listen(port);
console.log('Server running on port: ' + port);