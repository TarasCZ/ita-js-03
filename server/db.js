import knex from 'knex'
import {Model} from 'objection'

const db = knex({
  client: 'pg',
  connection: {
    host: 'localhost',
    user: 'postgres',
    password: 'postgres',
    database: 'contact'
  }
})

Model.knex(db)

class Base extends Model {
  get tableName() {
    return this.name.toLocaleLowerCase()
    
  }

  static async findById(id) {
    return await this.query().where({id: id}).limit(1).first();
  }
}

export class Contact extends Base {

}