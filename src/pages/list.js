import React, {Component} from 'react'
import { Link } from 'react-router-dom'
import Table from '../components/table' 
import { ContactsService } from '../contacts-service'


export default class List extends Component {
    constructor(){
        super()
        this.state = { contacts: [] }
    }

    componentWillMount() {
        this.load()
    }

    async load() {
        this.setState({
            contacts: await ContactsService.getContacts()
        })
    }

    render(){
        const contacts = this.state.contacts
        return(
            <div className="container">
                <h2>Contacts</h2>
                <div className="row">
                    <div className="col-md-4">
                        <Link to={'/contacts/new'} className="btn btn-default">+ New Contact</Link>
                    </div>
                    <div className="col-md-4"></div>
                    <div className="col-md-4">
                        <input type="text" className="form-control" placeholder="Search..."></input>
                    </div>
                </div>
                    
                <Table contacts={contacts} />
            </div>
        )
    }
}