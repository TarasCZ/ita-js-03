import React, { Component } from 'react'
import AlertBox from '../components/alertBox'

class Login extends Component{
    constructor(){  
        super()
        this.state={badPsw: false}
    }
    onLogin() {
        this.setState({badPsw: true})
        this.props.logIn(document.getElementById('usrPsw').value)
    }
    render(){
        
        return(
            <div className="container">
                <div className="row">
                    
                    <div className="col-md-offset-5 col-md-3">
                        <div className="well well-lg">
                            <div className="form-login">
                                <h4>Prosím zadejte 'heslo'</h4>
                                <div className="input-group">
                                    <input type="text" id="usrPsw" className="form-control chat-input" placeholder="password" />
                                    <span className="input-group-btn">     
                                        <button onClick={this.onLogin.bind(this)} className="btn btn-primary btn-md btn-default" type="button">Login</button>
                                    </span>
                                </div>
                            </div>
                            {this.state.badPsw ? <AlertBox text="Zadali jste špatné heslo!"/> : null}
                        </div>
                    </div>
               </div>
            </div>
        )
    }
}

export default Login