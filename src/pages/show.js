import React from 'react'
import { Link } from 'react-router-dom'


class Show extends React.Component {
    render(){
        return(
            <div className="container">
                <h2>Contact detail</h2>
                <div className="row">
                    <div className="col-md-6">
                        <div className="row">
                            <div className="col-md-4">Hajaja</div>
                            <div className="col-md-8">John Doe</div>
                        </div>

                        <div className="row">   
                            <div className="col-md-4">Phone</div>
                            <div className="col-md-8">...</div>
                        </div>

                        <div className="row">
                            <div className="col-md-4">Address</div>
                            <div className="col-md-8">...</div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="row">
                            <div className="col-md-4">Note</div>
                            <div className="col-md-8">...</div>
                        </div>                
                    </div>
                </div>

                <div>
                    <Link to="/contacts" className="btn btn-default">Edit</Link>
                    <Link to="/list" className="btn btn-default">Delete</Link>
                </div>
            </div>
        )
    }
}

export default Show