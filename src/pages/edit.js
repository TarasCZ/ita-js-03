import React, { Component } from 'react'
import EditForm from '../components/editForm'
import {ContactsService} from '../contacts-service'

class Edit extends Component {
    constructor(){
        super()
        this.state = {contact: ""}
    }

    componentWillMount() {
        if(this.props.match.params.id === "new"){
            this.setState(
                {contact: {
                    name: "",
                    phone:"",
                    address: "",
                    note: ""
                }})
        } else {
            this.load(this.props.match.params.id)
        }
    }
    componentWillReceiveProps(nextProps){
        this.load(nextProps.match.params.id)
    }
    async load(id) {
        this.setState({contact: await ContactsService.getContact(id)})
    }
    async save(contact){
        await ContactsService.save(contact)
        this.props.history.push('/contacts')
    }
    async delete(id){
        await ContactsService.delete(id)
        this.props.history.push('/contacts')
    }
    render(){
        return(
            <div className="container">
                {this.props.match.params.id === "new" ? <h2>New Contact</h2> : <h2>Edit Contact</h2>  }  
                {<EditForm contact={this.state.contact} save={this.save.bind(this)} delete={this.delete.bind(this)}/>}
                
                
            </div>
        
        )
    }
}

export default Edit