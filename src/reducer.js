import {List, Map} from 'immutable';

const initialState = {
  logged: false,
  contacts: [],
  contact: {
    name: "",
    phone: "",
    address: "",
    note: ""
  }
}

function setState(state, newState) {
  return state.merge(newState);
}

export default function(state = Map(initialState), action) {
  switch (action.type) {
  case 'SET_STATE':
    return setState(state, action.state);
  }
  return state;
}