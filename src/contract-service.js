import $http from 'axios'

export class ContactsService {
  static async getContacts() {
    return (await $http.get('http://localhost:1234/contracts')).data
  }

  static async getContact(id) {
    return (await $http.get(`http://localhost:1234/contracts/${id}`)).data
  }

  static async save(contact) {
    if (contact.id) {
      return this.update(contact)
    }

    return this.create(contact)
  }

  static async update(contact) {
    return (await $http.post(`http://localhost:1234/contracts/${contact.id}`, contact)).data
  }

  static async create(data) {
    return (await $http.post(`http://localhost:1234/contracts`, data))
  }
}
