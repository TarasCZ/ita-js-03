import React from 'react';
import { HashRouter as Router } from 'react-router-dom'
import { render } from 'react-dom';
import App from './app';
import 'bootstrap'
import './index.css';

const root = document.getElementById('root');

render(
    <Router>
        <App />
    </Router>,
root);

