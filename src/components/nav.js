import React from 'react'
import {Link} from 'react-router-dom'
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap'


const NavTop = onLogout => {
  return(
    <Navbar inverse collapseOnSelect>
      <Navbar.Header>
        <Navbar.Brand>
          <Link to='/contacts'><span>Contact List</span></Link>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav>
          <NavItem href="">Empty link</NavItem>
          <NavDropdown title="Dropdown" id="basic-nav-dropdown">
            <MenuItem>Action</MenuItem>
            <MenuItem>Another action</MenuItem>
            <MenuItem>Something else here</MenuItem>
            <MenuItem divider />
            <MenuItem>Separated link</MenuItem>
          </NavDropdown>
        </Nav>
              
        <Nav pullRight>
          <NavItem onClick={onLogout.logOut} href="/">Log out</NavItem>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}

export default NavTop