import React, {Component}  from 'react'
import PropTypes from 'prop-types'

export default class EditForm extends Component {
    constructor(){
        super()
        this.state = {contact: {}}
        this.onChange = this.onChange.bind(this)
    }
    componentWillReceiveProps(nextProp){
        this.setState({contact: nextProp.contact})
    }
    onChange(e){
        const contact = this.state.contact
        contact[e.target.name] = e.target.value 
        this.setState({contact})
    }

    save(){
        this.props.save(this.state.contact)
    }
    delete(){
        this.props.delete(this.state.contact.id)
    }
    render(){
        return(
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label htmlFor="">Name</label>
                            <input type="text" className="form-control" name="name" value={this.state.contact.name} onChange={this.onChange} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="">Phone</label>
                            <input type="text" className="form-control" name="phone" value={this.state.contact.phone} onChange={this.onChange} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="">Address</label>
                            <textarea className="form-control" name="address" value={this.state.contact.address} onChange={this.onChange}></textarea>
                        </div>
                        </div>
                        <div className="col-md-6">
                        <div className="form-group">
                            <label htmlFor="">Note</label>
                            <textarea className="form-control" name="note" value={this.state.contact.note} onChange={this.onChange}></textarea>
                        </div>
                    </div>
                </div>
                <div>
                    <button className="btn btn-default" onClick={this.save.bind(this)}>Save</button>
                    <button className="btn btn-default" onClick={this.delete.bind(this)}>Delete</button>
                </div>
            </div>
        )
    }
}

EditForm.propTypes = {
    contact: PropTypes.object
};
