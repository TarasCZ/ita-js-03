import React, {Component} from 'react'
import { Link } from 'react-router-dom'
import {ContactsService} from '../contacts-service'

const tableRow = (id,name,phone,address,note) => {
    return(
        <tr key={id}>
            <td><Link to={"/contacts/"+id}>{name}</Link></td>
            <td>{phone}</td>
            <td>{address}</td>
            <td>{note}</td>
        </tr>
    )
}

const renderRows = (props) => {
    const contacts = props
    const rows = []
    contacts.map((n) => rows.push(tableRow(n.id, n.name, n.phone, n.address, n.note)))
    return rows
}


class Table extends Component {  
    constructor(){
        super()
        this.state = { contacts: [] }
    }

    componentWillMount() {
        this.load()
    }

    async load() {
        this.setState({
            contacts: (await ContactsService.getContacts())
        })
    }
    
    render(){
        return(
            <table className='table table-striped'>
                <thead> 
                    <tr>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>Note</th>
                    </tr>
                </thead>
                <tbody>
                    { renderRows(this.state.contacts) }
                </tbody>
            </table>
        )
    }
}




export default Table