import React from 'react'
import PropTypes from 'prop-types'

const AlertBox = (text) => {
        return (<div className="alert alert-danger" role="alert" style={{marginBottom: 5,marginTop: 15}}>{text.text}</div>);
}

AlertBox.PropTypes = {
        text: PropTypes.string.isRequired
}

export default AlertBox