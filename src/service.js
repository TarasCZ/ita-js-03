import _ from 'lodash' 
import $http from 'axios' 

export class ContactsService {  
    static async getContacts() {    
        //const data = _.values((await $http.get('localhost:8081/contacts')).data)
        //return data
        return _.values((await $http.get('https://contacts-14b6b.firebaseio.com/contacts.json')).data)  
    }  
    static async getContact(id) {    
        return (await $http.get(`https://contacts-14b6b.firebaseio.com/contacts/${id}.json`)).data  
    }  
    static async save(contact) {    
        if (contact.id) {      
            return this.update(contact)    
        }    
        return this.create(contact)  
    }  
    static async update(contact) {    
        return (await $http.patch(`https://contacts-14b6b.firebaseio.com/contacts/${contact.id}.json`, contact)).data  
    }  
    static async create(data) {    
        const id = Date.now()    
        return (await $http.put(`https://contacts-14b6b.firebaseio.com/contacts/${id}.json`, _.assign(data, {id: id})))  
    } 
    static async delete(id) {
        await $http.delete(`https://contacts-14b6b.firebaseio.com/contacts/${id}.json`)
    }
}