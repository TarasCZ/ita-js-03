import React, {Component} from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Nav from './components/nav'
import List from './pages/list'
import Edit from './pages/edit'
import Login from './pages/login'

class App extends Component {
    constructor(){
        super()
        this.state = {logged: false}
    }

    logIn(password) {
        if(password==='heslo'){
            this.setState({logged: true})
        } 
        return this.state.logged
    }

    logOut() {
        this.setState({logged: false})
    }

    render() {

        return (
          <main>
              <Nav logOut = {this.logOut.bind(this)} />
            {
              (this.state.logged &&
              <Switch>
                <Route path="/contacts/:id" component={Edit} />
                <Route exact path="/contacts" component={List} />
                {<Redirect from="/" to="/contacts" />
                }
              </Switch>) || <Login logIn = {this.logIn.bind(this)} />
            }
          </main>
        )
    }

}

export default App;