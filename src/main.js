import React from 'react';
import {HashRouter as Router} from 'react-router-dom'
import { render } from 'react-dom';
import {Provider} from 'react-redux'
import {store} from './store'

import App from './jsx/App.jsx'

const app = document.getElementById('app');

render(
    <Provider store={store}>
        <Router>
            <App />
        </Router>
    </Provider>,
app);
