import React from 'react';
import 'babel-polyfill';
import { Navbar, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import Nav from '../components/nav'
import AlertBox from '../components/alertBox'
import Welcome from './Welcome';
import EditForm from '../components/editForm'
import Table from '../components/table'

const contact = {name: 'Anastazia Storybook', phone: '123 456 789', address: 'Storystreet', note: 'Vymyšlená osoba' }

storiesOf('Edit Form', module)
  .addDecorator(story =>
    <div className="container">
      {story()}
    </div>
  )
  .add('With Test Data', () => <EditForm contact = {contact} save={action('Save')} delete={action('Delete')}/>)
  .add('Empty', () => <EditForm contact = ""/>)

storiesOf('Alert Box', module)
  .add("Red Alert", () => <AlertBox text="This is red warning!"/>)

storiesOf('Table', module)
  .addDecorator(story =>
    <div className="container">
      {story()}
    </div>
  )
  .add('With Data', () => <Table/>)
  storiesOf('Navigace', module)
    .add("Navigace", () => <Nav logOut={action('LogOut')}/>)
